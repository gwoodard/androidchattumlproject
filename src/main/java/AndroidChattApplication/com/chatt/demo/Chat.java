package AndroidChattApplication.com.chatt.demo;
import java.util.LinkedList;
import genmymodelreverse.android.widget.BaseAdapter;
import java.util.List;
import AndroidChattApplication.com.chatt.demo.model.Conversation;
import AndroidChattApplication.com.chatt.demo.model.Conversation;
import AndroidChattApplication.com.chatt.demo.custom.CustomActivity;
import genmymodelreverse.java.util.Date;


/**
 * The Class Chat is the Activity class that holds main chat screen. It showsall the conversation messages between two users and also allows the user tosend and receive messages.
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Chat extends CustomActivity
{
	/**
	 * The Editext to compose the message. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private android.widget.EditText txt;
	
	/**
	 * The user name of buddy. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String buddy;
	
	/**
	 * The date of last message in conversation. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private java.util.Date lastMsgDate;
	
	/**
	 * Flag to hold if the activity is running or not. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private boolean isRunning;
	
	/**
	 * The handler. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private static android.os.Handler handler;
	
	/**
	 * The Conversation list. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private List<Conversation> convList;
	
	/**
	 * The chat adapter. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private ChatAdapter adp;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public Chat(){
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected void onCreate(android.os.Bundle savedInstanceState) {
		// TODO implement me	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected void onResume() {
		// TODO implement me	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected void onPause() {
		// TODO implement me	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public void onClick(android.view.View v) {
		// TODO implement me	
	}
	
	/**
	 * Call this method to Send message to opponent. It does nothing if the textis empty otherwise it creates a Parse object for Chat message and send itto Parse server.
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private void sendMessage() {
		// TODO implement me	
	}
	
	/**
	 * Load the conversation list from Parse server and save the date of lastmessage that will be used to load only recent new messages
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private void loadConversationList() {
		// TODO implement me	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		// TODO implement me
		return false;	
	}
	
	/**
	 * The Class ChatAdapter is the adapter class for Chat ListView. Thisadapter shows the Sent or Receieved Chat message in each list item.
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	
	class ChatAdapter extends android.widget.BaseAdapter
	{
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 */
		public ChatAdapter(){
			super();
		}
	
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public int getCount() {
			// TODO implement me
			return 0;	
		}
		
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public Conversation getItem(int arg0) {
			// TODO implement me
			return null;	
		}
		
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public long getItemId(int arg0) {
			// TODO implement me
			return 0L;	
		}
		
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public android.view.View getView(int pos, android.view.View v, android.view.ViewGroup arg2) {
			// TODO implement me
			return null;	
		}
		
	}
	}

