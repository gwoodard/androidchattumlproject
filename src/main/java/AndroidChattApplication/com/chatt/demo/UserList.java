package AndroidChattApplication.com.chatt.demo;
import java.util.LinkedList;
import genmymodelreverse.android.widget.BaseAdapter;
import java.util.List;
import AndroidChattApplication.com.chatt.demo.custom.CustomActivity;


/**
 * The Class UserList is the Activity class. It shows a list of all users ofthis app. It also shows the Offline/Online status of users.
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class UserList extends CustomActivity
{
	/**
	 * The Chat list. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private List<com.parse.ParseUser> uList;
	
	/**
	 * The user. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static com.parse.ParseUser user;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public UserList(){
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected void onCreate(android.os.Bundle savedInstanceState) {
		// TODO implement me	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected void onDestroy() {
		// TODO implement me	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected void onResume() {
		// TODO implement me	
	}
	
	/**
	 * Update user status.
	 * @paramonlinetrue if user is online
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private void updateUserStatus(boolean online) {
		// TODO implement me	
	}
	
	/**
	 * Load list of users.
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private void loadUserList() {
		// TODO implement me	
	}
	
	/**
	 * The Class UserAdapter is the adapter class for User ListView. Thisadapter shows the user name and it's only online status for each item.
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	
	class UserAdapter extends android.widget.BaseAdapter
	{
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 */
		public UserAdapter(){
			super();
		}
	
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public int getCount() {
			// TODO implement me
			return 0;	
		}
		
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public com.parse.ParseUser getItem(int arg0) {
			// TODO implement me
			return null;	
		}
		
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public long getItemId(int arg0) {
			// TODO implement me
			return 0L;	
		}
		
		/**
		 * <!-- begin-user-doc -->
		 * <!--  end-user-doc  -->
		 * @generated
		 * @ordered
		 */
		
		public android.view.View getView(int pos, android.view.View v, android.view.ViewGroup arg2) {
			// TODO implement me
			return null;	
		}
		
	}
	}

