package AndroidChattApplication.com.chatt.demo.custom;
import genmymodelreverse.android.view.View.OnClickListener;
import AndroidChattApplication.com.chatt.demo.utils.TouchEffect;
import genmymodelreverse.android.support.v4.app.FragmentActivity;


/**
 * This is a common activity that all other activities of the app can extend toinherit the common behaviors like implementing a common interface that can beused in all child activities.
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class CustomActivity extends android.support.v4.app.FragmentActivity implements android.view.View.OnClickListener
{
	/**
	 * Apply this Constant as touch listener for views to provide alpha toucheffect. The view must have a Non-Transparent background.
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static final TouchEffect TOUCH = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public CustomActivity(){
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public void setContentView(int layoutResID) {
		// TODO implement me	
	}
	
	/**
	 * This method will setup the top title bar (Action bar) content and displayvalues. It will also setup the custom background theme for ActionBar. Youcan override this method to change the behavior of ActionBar forparticular Activity
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	protected void setupActionBar() {
		// TODO implement me	
	}
	
	/**
	 * Sets the touch and click listener for a view with given id.
	 * @paramidthe id
	 * @return the view on which listeners applied
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public android.view.View setTouchNClick(int id) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Sets the click listener for a view with given id.
	 * @paramidthe id
	 * @return the view on which listener is applied
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public android.view.View setClick(int id) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public void onClick(android.view.View v) {
		// TODO implement me	
	}
	
}

