package AndroidChattApplication.com.chatt.demo.utils;


/**
 * The Class Utils is a common class that hold many kind of different usefulutility methods.
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Utils
{
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public Utils(){
		super();
	}

	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @parambtn1the btn1
	 * @parambtn2the btn2
	 * @paramlistener1the listener1
	 * @paramlistener2the listener2
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, String msg, String btn1, String btn2, DialogInterface.OnClickListener listener1, DialogInterface.OnClickListener listener2) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @parambtn1the btn1
	 * @parambtn2the btn2
	 * @paramlistener1the listener1
	 * @paramlistener2the listener2
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, int msg, int btn1, int btn2, DialogInterface.OnClickListener listener1, DialogInterface.OnClickListener listener2) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @parambtn1the btn1
	 * @parambtn2the btn2
	 * @paramlistenerthe listener
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, String msg, String btn1, String btn2, DialogInterface.OnClickListener listener) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @parambtn1the btn1
	 * @parambtn2the btn2
	 * @paramlistenerthe listener
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, int msg, int btn1, int btn2, DialogInterface.OnClickListener listener) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @paramlistenerthe listener
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, String msg, DialogInterface.OnClickListener listener) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @paramlistenerthe listener
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, int msg, DialogInterface.OnClickListener listener) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, String msg) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @parammsgthe msg
	 * @return the alert dialog
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static android.app.AlertDialog showDialog(android.content.Context ctx, int msg) {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Show dialog.
	 * @paramctxthe ctx
	 * @paramtitlethe title
	 * @parammsgthe msg
	 * @paramlistenerthe listener
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static void showDialog(android.content.Context ctx, int title, int msg, DialogInterface.OnClickListener listener) {
		// TODO implement me	
	}
	
	/**
	 * Hide keyboard.
	 * @paramctxthe ctx
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static final void hideKeyboard(android.app.Activity ctx) {
		// TODO implement me	
	}
	
	/**
	 * Hide keyboard.
	 * @paramctxthe ctx
	 * @paramvthe v
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static final void hideKeyboard(android.app.Activity ctx, android.view.View v) {
		// TODO implement me	
	}
	
}

