package AndroidChattApplication.com.chatt.demo.model;
import genmymodelreverse.java.util.Date;


/**
 * The Class Conversation is a Java Bean class that represents a single chatconversation message.
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Conversation
{
	/**
	 * The Constant STATUS_SENDING. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static final int STATUS_SENDING = 0;
	
	/**
	 * The Constant STATUS_SENT. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static final int STATUS_SENT = 0;
	
	/**
	 * The Constant STATUS_FAILED. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static final int STATUS_FAILED = 0;
	
	/**
	 * The msg. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String msg;
	
	/**
	 * The status. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private int status;
	
	/**
	 * The date. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private java.util.Date date;
	
	/**
	 * The sender. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String sender;
	

	/**
	 * Instantiates a new conversation.
	 * @parammsgthe msg
	 * @paramdatethe date
	 * @paramsenderthe sender
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public Conversation(String msg, java.util.Date date, String sender) {
		super();
		// TODO construct me	
	}
	
	/**
	 * Instantiates a new conversation.
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public Conversation() {
		super();
		// TODO construct me	
	}
	
	/**
	 * Gets the msg.
	 * @return the msg
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public String getMsg() {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Sets the msg.
	 * @parammsgthe new msg
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public void setMsg(String msg) {
		// TODO implement me	
	}
	
	/**
	 * Checks if is sent.
	 * @return true, if is sent
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public boolean isSent() {
		// TODO implement me
		return false;	
	}
	
	/**
	 * Gets the date.
	 * @return the date
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public java.util.Date getDate() {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Sets the date.
	 * @paramdatethe new date
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public void setDate(java.util.Date date) {
		// TODO implement me	
	}
	
	/**
	 * Gets the sender.
	 * @return the sender
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public String getSender() {
		// TODO implement me
		return null;	
	}
	
	/**
	 * Sets the sender.
	 * @paramsenderthe new sender
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public void setSender(String sender) {
		// TODO implement me	
	}
	
	/**
	 * Gets the status.
	 * @return the status
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public int getStatus() {
		// TODO implement me
		return 0;	
	}
	
	/**
	 * Sets the status.
	 * @paramstatusthe new status
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public void setStatus(int status) {
		// TODO implement me	
	}
	
}

