package AndroidChattApplication.com.chatt.demo.utils;


/**
 * The Class Const is a single common class to hold all the app Constants.
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Const
{
	/**
	 * The Constant EXTRA_DATA. 
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public static final String EXTRA_DATA = "";
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public Const(){
		super();
	}

}

